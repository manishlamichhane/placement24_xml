# A Python Application to read xml files and parse nodes / attributes from it
# Since a same parser is used to parse 3 different type of xml file at once, a bit of user-defined conditions are used in parse_language() method of BParser
# In real life the condition suitable for specific input file is used. This practice is only for demonstration purpose. Else this kind of coding practise is not considered an acceptable one

# Files Description

	A. Parser.py

	The file contains a class that has method specific to parsing a xml file. It contains following methods:

		1. load()

		Loads any xml file. The path to the file is passed as parameter

		2. parse_node()

		Parses any given node and returns a list of the inner text data. The node is taken as an argument

		3. parse_attribute()

		Parses any give node and returns a list of values of attributes in the node. The node and the attribute name is taken as argument


	B. BParser.py

	The file extends the Parser class. It is an implementation of the Parser class in this specific scenario.

	C. main.py

	This file is where all the object instantiation takes place. Something like a main class in Java


# Dependency
	
	1. Langdetect

	To detect the language of text, a python library ##Langdetect is used. To install langdetect:

	##sudo pip install langdetect

# How to run?

##python main.py

NOTE: The path of the xml file to be supplied should be in unix format (either absolute path from root or just the file name if in same directory)
For convenience, please put all 3 xml file in the same directory as the python files


