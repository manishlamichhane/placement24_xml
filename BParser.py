import xml.etree.cElementTree as etree #the cElementTree is a c implementation of Element Tree; is much faster and consumes less memory
from Parser import Parser as Base
from langdetect import detect
# from sys import exit


class BParser(Base):

	def __init__(self):
		# instantiating a job list
		self.job_list = []
		self.lang_list = []
		super(BParser,self).load()
		try:
			self.tree = etree.parse(self.file_location)
		except etree.ParseError:
			#preprocess the file before as there is some unusual characters
			#and recursively parse through the job url
			print "XML file needs preprocessing. Preprocessing now...."
			super(BParser,self).clean_html_special_chars()

	#parses the job url from the 'url' node of the xml input file
	def parse_job(self):
		
		# if the parsing is successful and the job_list has a list of urls
		# This holds true for feed_a.xml and feed_c.xml
		self.job_list = super(BParser,self).parse_node('url')
		if len(self.job_list):
			print "Job list: ",self.job_list
		# feed_b.xml has the job url in a different name i.e. homepageOwner
		else:
			self.job_list = super(BParser,self).parse_attribute('homepage','homepageOwner')
			print "Job list: ",self.job_list

	#parses the lang node from the xml file
	def parse_language(self):
		if self.file_location == 'feed_b.xml':
	 		self.lang_list = super(BParser,self).parse_attribute('PositionProfile','lang')
	 	elif self.file_location == 'feed_c.xml':
		 	lang_list = super(BParser,self).parse_node('role')
	 		for lang in lang_list:
	 			self.lang_list.append(detect(lang))
	 	else:
	 		for node in self.tree.iter('description'):
				self.lang_list.append(detect(etree.tostring(node,encoding="us-ascii",method="xml")))
		print "Language List(respective to job url display order):",self.lang_list
	 	
	 	




