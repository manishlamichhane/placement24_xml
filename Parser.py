from abc import ABCMeta, abstractmethod
from sys import exit
import os,sys
import re

if sys.version_info[0] < 3:
    ABC = ABCMeta('ABC', (), {})
else:
    ABC = ABC

class Parser(ABC):

	def load(self):
		#loads the file to parse
		file_location = raw_input("Enter the xml file path: ")
		if os.path.exists(file_location):
			self.file_location = file_location
		else:
			print "File not found"
			exit()

	#removes html special characters from a file
	def clean_html_special_chars(self):
		regex = '&.*?;';
		f = open(self.file_location,"r+")
		cleaned_xml = re.sub(regex,'',f.read())
		f.seek(0)
		f.truncate()
		f.write(cleaned_xml)
		f.close()


	#parses the text value from the node provided in argument
	def parse_node(self,root_node):
		result_list = []
		for node in self.tree.iter(root_node):
		 	result_list.append(node.text)
		return result_list

	#parses the attribute provided in argument from the node provided in argument
	def parse_attribute(self,node_root,attribute):
		result_list = []
		for node in self.tree.iter(node_root):
			result_list.append(node.attrib[attribute])
		return result_list
		